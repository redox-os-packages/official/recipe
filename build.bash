source ../common.bash

git clone --single-branch --depth 1 "https://github.com/gibbz00/$PACKAGE_NAME"
cd "$PACKAGE_NAME"
cargo build --release
